const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit"
const VowelMan = ["aa", "ee", "ii", "oo", "uu"]

//1// Display an array from the cities in gotCitiesCSV.
newElement = document.createElement("h1")
sar = document.createTextNode("kataz1")
newElement.appendChild(sar)
document.getElementById("sauce").appendChild(newElement)

SplitnerCell = gotCitiesCSV.split(',')


newElement = document.createElement("p")
out = document.createTextNode(JSON.stringify(SplitnerCell))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(out)
document.getElementById("sauce").appendChild(newElement)

//2// Display an array of words from the sentence in bestThing.
newElement = document.createElement("h2")
oo = document.createTextNode("kataz2")
newElement.appendChild(oo)
document.getElementById("sauce").appendChild(newElement)

BinderCell = bestThing.split(' ')

newElement = document.createElement("p")
boogar = document.createTextNode(JSON.stringify(BinderCell))
// newElement.textContent = JSON.stringify(bestThing)
newElement.appendChild(boogar)
document.getElementById("sauce").appendChild(newElement)
//3// Display a string separated by semi-colons instead of commas from gotCitiesCSV.
splinter  = gotCitiesCSV.split(',').join('; ')

newElement = document.createElement("h1")
tt = document.createTextNode("kataz3")
newElement.appendChild(tt)
document.getElementById("sauce").appendChild(newElement)

newElement = document.createElement("p")
boogar = document.createTextNode(splinter)
// newElement.textContent = JSON.stringify(splinter)
newElement.appendChild(boogar)
document.getElementById("sauce").appendChild(newElement)

//4// Display a CSV (comma-separated) string from the lotrCitiesArray.

newElement = document.createElement("h2")
sar = document.createTextNode("kataz4")
newElement.appendChild(sar)
document.getElementById("sauce").appendChild(newElement)



newElement = document.createElement("p")
out = document.createTextNode(JSON.stringify(lotrCitiesArray))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(out)
document.getElementById("sauce").appendChild(newElement)


//5// Display the first 5 cities in lotrCitiesArray.

newElement = document.createElement("h1")
lob = document.createTextNode("kataz5")
newElement.appendChild(lob)
document.getElementById("sauce").appendChild(newElement)


splinta = lotrCitiesArray.slice(0,5)
newElement = document.createElement("p")
Mini = document.createTextNode(splinta)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(Mini)
document.getElementById("sauce").appendChild(newElement)

//6// Display the last 5 cities in lotrCitiesArray.

newElement = document.createElement("h2")
bol = document.createTextNode("kataz6")
newElement.appendChild(bol)
document.getElementById("sauce").appendChild(newElement)


splinterz = lotrCitiesArray.slice(3)
newElement = document.createElement("p")
inim = document.createTextNode(splinterz)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inim)
document.getElementById("sauce").appendChild(newElement)

//7// Display the 3rd to 5th city in lotrCitiesArray.
newElement = document.createElement("h1")
bole = document.createTextNode("kataz7")
newElement.appendChild(bole)
document.getElementById("sauce").appendChild(newElement)


splinterino = lotrCitiesArray.slice(2,5)
newElement = document.createElement("p")
inim = document.createTextNode(splinterino)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inim)
document.getElementById("sauce").appendChild(newElement)

//8// Using Array.prototype.splice(), remove "Rohan" from lotrCitiesArray.
newElement = document.createElement("h2")
eng = document.createTextNode("kataz8")
newElement.appendChild(eng)
document.getElementById("sauce").appendChild(newElement)


 lotrCitiesArray.splice(2,1)
newElement = document.createElement("p")
boos = document.createTextNode(lotrCitiesArray)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(boos)
document.getElementById("sauce").appendChild(newElement)
//9// Using Array.prototype.splice(), remove all cities after "Dead Marshes" in lotrCitiesArray.
newElement = document.createElement("h2")
bolek = document.createTextNode("kataz9")
newElement.appendChild(bolek)
document.getElementById("sauce").appendChild(newElement)


lotrCitiesArray.splice(5)
newElement = document.createElement("p")
inimz = document.createTextNode(lotrCitiesArray)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inimz)
document.getElementById("sauce").appendChild(newElement)

//10// Using Array.prototype.splice(), add "Rohan" back to lotrCitiesArray right after "Gondor".
newElement = document.createElement("h2")
loc = document.createTextNode("kataz10")
newElement.appendChild(loc)
document.getElementById("sauce").appendChild(newElement)


lotrCitiesArray.splice(2, 0,'Rohan')
newElement = document.createElement("p")
inix = document.createTextNode(lotrCitiesArray)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inix)
document.getElementById("sauce").appendChild(newElement)

//11// Using Array.prototype.splice(), rename "Dead Marshes" to "Deadest Marshes" in lotrCitiesArray.
newElement = document.createElement("h2")
boloon = document.createTextNode("kataz11")
newElement.appendChild(boloon)
document.getElementById("sauce").appendChild(newElement)


lotrCitiesArray.splice(5, 1, "Deadest Marshes")
newElement = document.createElement("p")
inim = document.createTextNode(lotrCitiesArray)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inim)
document.getElementById("sauce").appendChild(newElement)

//12// Using String.prototype.slice(), display the first 14 characters from bestThing.
newElement = document.createElement("h2")
bolcheese = document.createTextNode("kataz12")
newElement.appendChild(bolcheese)
document.getElementById("sauce").appendChild(newElement)


splinterzz = bestThing.slice(0,14)
newElement = document.createElement("p")
inimi = document.createTextNode(splinterzz)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inimi)
document.getElementById("sauce").appendChild(newElement)
//13// Using String.prototype.slice(), display the last 12 characters from bestThing.
newElement = document.createElement("h2")
chilicheese = document.createTextNode("kataz13")
newElement.appendChild(chilicheese)
document.getElementById("sauce").appendChild(newElement)


splinterzZ = bestThing.slice(-14)
newElement = document.createElement("p")
inimii = document.createTextNode(splinterzZ)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inimii)
document.getElementById("sauce").appendChild(newElement)


//14// Using String.prototype.slice(), display characters between the 23rd and 38th position of bestThing (i.e. "boolean is even").
newElement = document.createElement("h2")
kraftcheese = document.createTextNode("kataz14")
newElement.appendChild(kraftcheese)
document.getElementById("sauce").appendChild(newElement)


splinterzQ = bestThing.slice(23,38)
newElement = document.createElement("p")
inimiQ = document.createTextNode(splinterzQ)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inimiQ)
document.getElementById("sauce").appendChild(newElement)

//15// Repeat #13 using String.prototype.substring() instead of String.prototype.slice().
newElement = document.createElement("h2")
bloccheese = document.createTextNode("kataz15")
newElement.appendChild(bloccheese)
document.getElementById("sauce").appendChild(newElement)


splinteriz = bestThing.substring(67)
newElement = document.createElement("p")
inimiP = document.createTextNode(splinteriz)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inimiP)
document.getElementById("sauce").appendChild(newElement)

//16// Repeat #14 using String.prototype.substr() instead of String.prototype.slice().
newElement = document.createElement("h2")
facepalmcheese = document.createTextNode("kataz16")
newElement.appendChild(facepalmcheese)
document.getElementById("sauce").appendChild(newElement)


splinterzL = bestThing.substring(23,38)
newElement = document.createElement("p")
iniL = document.createTextNode(splinterzL)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(iniL)
document.getElementById("sauce").appendChild(newElement)


//17// Find and display the index of "only" in bestThing.

newElement = document.createElement("h2")
bluecheese = document.createTextNode("kataz17")
newElement.appendChild(bluecheese)
document.getElementById("sauce").appendChild(newElement)


splintA = bestThing.substring(63, 69)
newElement = document.createElement("p")
inax = document.createTextNode(splintA)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inax)
document.getElementById("sauce").appendChild(newElement)

//18// Find and display the index of the last word in bestThing.
newElement = document.createElement("h2")
guac = document.createTextNode("kataz18")
newElement.appendChild(guac)
document.getElementById("sauce").appendChild(newElement)


splintAcell = bestThing.substring(78)
newElement = document.createElement("p")
inaxy = document.createTextNode(splintAcell)
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inaxy)
document.getElementById("sauce").appendChild(newElement)

//19// Find and display all cities from gotCitiesCSV that use double vowels ("aa", "ee", …).
newElement = document.createElement("h2")
cougar = document.createTextNode("kataz19")
newElement.appendChild(cougar)
document.getElementById("sauce").appendChild(newElement)

RayMan = []
for(i=0; i < SplitnerCell.length; i++){
    for(k=0; k<SplitnerCell[i].length-1; k++){
        for(j=0; j<VowelMan.length; j++){

            if(SplitnerCell[i][k] + SplitnerCell[i][k+1] == VowelMan[j]){
                if(!RayMan.includes(SplitnerCell[i]))
                    RayMan.push(SplitnerCell[i]) 
                           
        }
        
            }
    }
}
 sword = RayMan
newElement = document.createElement("p")
inaxye = document.createTextNode(JSON.stringify(sword))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inaxye)
document.getElementById("sauce").appendChild(newElement)


//20// Find and display all cities from lotrCitiesArray that end with "or".

Matrix=[]
newElement = document.createElement("h2")
gar = document.createTextNode("kataz20")
newElement.appendChild(gar)
document.getElementById("sauce").appendChild(newElement)

for(i=0; i < lotrCitiesArray.length; i++){
    const cur_val = lotrCitiesArray[i]
    if(cur_val.slice(cur_val.length-2, cur_val.length) == "or"){
        Matrix.push(cur_val)
    }
}

pword = Matrix
newElement = document.createElement("p")
inaxyre = document.createTextNode(JSON.stringify(pword))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inaxyre)
document.getElementById("sauce").appendChild(newElement)

//21// Find and display all the words in bestThing that start with a "b".
Matrix2=[]

for(i=0; i < BinderCell.length; i++){
    const result = BinderCell[i]
    if(result[0] == "b" ){
        Matrix2.push(result)
    }
}

newElement = document.createElement("h2")
rag = document.createTextNode("kataz21")
newElement.appendChild(rag)
document.getElementById("sauce").appendChild(newElement)

ord = Matrix2
newElement = document.createElement("p")
inaxyree = document.createTextNode(JSON.stringify(ord))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(inaxyree)
document.getElementById("sauce").appendChild(newElement)



//22// Display "Yes" or "No" if lotrCitiesArray includes "Mirkwood".
Matrix3=[]

for(i=0; i < lotrCitiesArray.length; i++){
    const result = "Yes" 
    const resultt = "no"
    const mari = lotrCitiesArray[i]
    if(mari == "Mirkwood"){
        Matrix3.push(result)
    } else {
        Matrix3.push(resultt)
    }
}

newElement = document.createElement("h2")
gag = document.createTextNode("kataz22")
newElement.appendChild(gag)
document.getElementById("sauce").appendChild(newElement)

Nord = Matrix3
newElement = document.createElement("p")
imort = document.createTextNode(JSON.stringify(Nord))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(imort)
document.getElementById("sauce").appendChild(newElement)

//23// Display "Yes" or "No" if lotrCitiesArray includes "Hollywood".
Matrix4=[]

for(i=0; i < lotrCitiesArray.length; i++){
    const result = "Yes" 
    const resultt = "no"
    const mari = lotrCitiesArray[i]
    if(mari == "Hollywood"){
        Matrix4.push(result)
    } else {
        Matrix4.push(resultt)
    }
}

newElement = document.createElement("h2")
trap = document.createTextNode("kataz23")
newElement.appendChild(trap)
document.getElementById("sauce").appendChild(newElement)

bord = Matrix4
newElement = document.createElement("p")
mort = document.createTextNode(JSON.stringify(bord))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mort)
document.getElementById("sauce").appendChild(newElement)


//24// Display the index of "Mirkwood" in lotrCitiesArray.

newElement = document.createElement("h2")
rap = document.createTextNode("kataz24")
newElement.appendChild(rap)
document.getElementById("sauce").appendChild(newElement)

cord = lotrCitiesArray[4].split('')
newElement = document.createElement("p")
mortimer = document.createTextNode(JSON.stringify(cord))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimer)
document.getElementById("sauce").appendChild(newElement)


//25// Find and display the first city in lotrCitiesArray that has more than one word.
newElement = document.createElement("h2")
par = document.createTextNode("kataz25")
newElement.appendChild(par)
document.getElementById("sauce").appendChild(newElement)

snasige = lotrCitiesArray[5]
newElement = document.createElement("p")
mortimerz = document.createTextNode(JSON.stringify(snasige))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimerz)
document.getElementById("sauce").appendChild(newElement)

//26// Reverse the order in lotrCitiesArray.

newElement = document.createElement("h2")
p = document.createTextNode("kataz26")
newElement.appendChild(p)
document.getElementById("sauce").appendChild(newElement)


newElement = document.createElement("p")
mortime = document.createTextNode(JSON.stringify(lotrCitiesArray.reverse(lotrCitiesArray)))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortime)
document.getElementById("sauce").appendChild(newElement)

//27// Sort lotrCitiesArray alphabetically.


newElement = document.createElement("h2")
pin = document.createTextNode("kataz27")
newElement.appendChild(pin)
document.getElementById("sauce").appendChild(newElement)


newElement = document.createElement("p")
mortimeplz = document.createTextNode(JSON.stringify(lotrCitiesArray.sort()))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimeplz)
document.getElementById("sauce").appendChild(newElement)


//28// Sort lotrCitiesArray by the number of characters in each city (i.e., shortest city names go first).


newElement = document.createElement("h2")
nip = document.createTextNode("kataz28")
newElement.appendChild(nip)
document.getElementById("sauce").appendChild(newElement)


newElement = document.createElement("p")
mortimeplzz = document.createTextNode(JSON.stringify(lotrCitiesArray.sort(function(a, b){
    return b.length - a.length;
})))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimeplzz)
document.getElementById("sauce").appendChild(newElement)

//29// Using Array.prototype.pop(), remove the last city from lotrCitiesArray.
newElement = document.createElement("h2")
nine = document.createTextNode("kataz29")
newElement.appendChild(nine)
document.getElementById("sauce").appendChild(newElement)

lotrCitiesArray.pop()
newElement = document.createElement("p")
mortimeplax = document.createTextNode(JSON.stringify(lotrCitiesArray))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimeplax)
document.getElementById("sauce").appendChild(newElement)


//30// Using Array.prototype.push(), add back the city from lotrCitiesArray that was removed in #29 to the back of the array.

newElement = document.createElement("h2")
dine = document.createTextNode("kataz30")
newElement.appendChild(dine)
document.getElementById("sauce").appendChild(newElement)

lotrCitiesArray.push("Rohan")
newElement = document.createElement("p")
mortimeplush = document.createTextNode(JSON.stringify(lotrCitiesArray))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimeplush)
document.getElementById("sauce").appendChild(newElement)

//31// Using Array.prototype.shift(), remove the first city from lotrCitiesArray.

newElement = document.createElement("h2")
pine = document.createTextNode("kataz31")
newElement.appendChild(pine)
document.getElementById("sauce").appendChild(newElement)

lotrCitiesArray.shift()
newElement = document.createElement("p")
mortimepile = document.createTextNode(JSON.stringify(lotrCitiesArray))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimepile)
document.getElementById("sauce").appendChild(newElement)
//32// Using Array.prototype.unshift(), add back the city from lotrCitiesArray that was removed in #31 to the front of the array.

newElement = document.createElement("h2")
pinez = document.createTextNode("kataz32")
newElement.appendChild(pinez)
document.getElementById("sauce").appendChild(newElement)

lotrCitiesArray.unshift("Deadest Marshes")

newElement = document.createElement("p")
mortimepilez = document.createTextNode(JSON.stringify(lotrCitiesArray))
// newElement.textContent = JSON.stringify(gotCitiesCSV)
newElement.appendChild(mortimepilez)
document.getElementById("sauce").appendChild(newElement)